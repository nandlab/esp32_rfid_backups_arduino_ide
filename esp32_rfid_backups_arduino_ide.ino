#include <Arduino.h>
#include <SPI.h>
#include <MFRC522.h>

#define LED 1
#define SERIAL_DEBUG_OUT
#define UID_BACKUPS 2
#define NUM_CORRECT_CARDS_ENOUGH 3

// Here only UID will be compared and SAK ignored
bool uidEqual(const MFRC522::Uid &uid1, const MFRC522::Uid &uid2) {
  return (
      uid1.size == uid2.size
    && !memcmp(uid1.uidByte, uid2.uidByte, uid1.size)
  );
}
// str needs to have a size of at least 30
void uidToStr(char str[], const MFRC522::Uid & uid) {
  str[0] = '\0';
  for (byte i=0; i<uid.size; i++) {
    if (i) {
      strcat(str, " ");
    }
    sprintf(str + strlen(str), "%02x", uid.uidByte[i]);
  }
}
bool CardPresent(MFRC522 &mfrc) {
  return mfrc.PICC_ReadCardSerial() || (mfrc.PICC_IsNewCardPresent() && mfrc.PICC_ReadCardSerial());
}
void setup() {}
// If something goes terribly wrong, the program resets.
void loop() {
  const unsigned long BAUD_RATE = 115200;
  /*
  const size_t NUM_OF_MFRCS = 2;
  const uint8_t MFRC_SS_PINS[NUM_OF_MFRCS] = {5, 17}; // nano: {8, 7};
  const uint8_t MFRC_RST_PIN = 4; // nano: 9;
  const uint8_t SW_PIN = 15;
  const uint8_t LED_RED = 16;
  const uint8_t LED_GREEN = 2;
  */
#ifdef SERIAL_LED
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH);
  delay(1000);
  digitalWrite(LED, LOW);
  delay(1000);
  digitalWrite(LED, HIGH);
  delay(1000);
  digitalWrite(LED, LOW);
  delay(1000);
  digitalWrite(LED, HIGH);
  // delay(1000);
#endif
  const uint8_t RIDDLE_START_PIN = 33;
  const uint8_t RIDDLE_SOLVED_PIN = 25;
  const size_t NUM_OF_MFRCS = 4;
  const uint8_t MFRC_SS_PINS[NUM_OF_MFRCS] = {15, 2, 4, 5};
  const uint8_t MFRC_RST_PIN = 22;
  const uint8_t LED_RED = 14;
  const uint8_t LED_GREEN = 27;
  bool correctCardTapped[NUM_OF_MFRCS] = {}; // Zero initialization
  int numCardsCorrect;
  MFRC522 *mfrc522s[NUM_OF_MFRCS];
  MFRC522::Uid correctUids[NUM_OF_MFRCS][UID_BACKUPS] = {
      {
          {4, {0x01, 0xFD, 0xA5, 0x25}, 0},
          {4, {0xD3, 0x1B, 0x5D, 0x16}, 0}
      },
      {
          {4, {0xB3, 0xBD, 0x61, 0x16}, 0},
          {4, {0x73, 0x7F, 0x67, 0x16}, 0}
      },
      {
          {4, {0x83, 0xEB, 0x68, 0x16}, 0},
          {4, {0x76, 0x82, 0x10, 0x29}, 0}
      },
      {
          {4, {0xD3, 0x18, 0x64, 0x16}, 0},
          {4, {0x53, 0xE9, 0x77, 0x16}, 0}
      }
  };
  char buf[512];
  pinMode(RIDDLE_START_PIN, INPUT_PULLUP);
  pinMode(RIDDLE_SOLVED_PIN, INPUT_PULLUP);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
#ifdef SERIAL_DEBUG_OUT
  Serial.begin(BAUD_RATE);  // Initialize serial communications with the PC
  while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
#endif
  SPI.begin();      // Init SPI bus
  for (size_t i=0; i<NUM_OF_MFRCS; i++) {
    mfrc522s[i] = new MFRC522(MFRC_SS_PINS[i], MFRC_RST_PIN);
    if (!mfrc522s[i]) {
#ifdef SERIAL_DEBUG_OUT
      Serial.println("Failed to allocate MFRC522 object!");
#endif
      digitalWrite(LED_RED, HIGH);
      while (true);
    }
    mfrc522s[i]->PCD_Init();    // Init MFRC522
  }
  /*
  delay(10);
  for (size_t i=0; i<NUM_OF_MFRCS; i++) {
    mfrc522s[i]->PCD_SetAntennaGain(B101<<4);
  }
  */
  delay(10);        // Optional delay. Some board do need more time after init to be ready, see Readme
#ifdef SERIAL_DEBUG_OUT
  for (size_t i=0; i<NUM_OF_MFRCS; i++) {
    Serial.print("MFRC ");
    Serial.println(i);
    mfrc522s[i]->PCD_DumpVersionToSerial(); // Show details of PCD - MFRC522 Card Reader details
    Serial.println(mfrc522s[i]->PCD_GetAntennaGain());
  }
  for (size_t i=0; i<NUM_OF_MFRCS; i++) {
    sprintf(buf, "The correct UIDs for MFRC %d are:  ", i);
    for (size_t j=0; j<UID_BACKUPS; j++) {
      if (j) {
        strcat(buf, ";  ");
      }
      uidToStr(buf + strlen(buf), correctUids[i][j]);
    }
    strcat(buf, "\r\n");
    // strcat(buf, "\r\nPlease tap the card for access...\r\n\r\n");
    Serial.print(buf);
  }
  Serial.println();
#endif
//  strcpy(buf, "The correct UID is: ");
//  uidToStr(buf + strlen(buf), correctUids[i]);
//  strcat(buf, "\r\nPlease tap the card for access...\r\n\r\n");
//  Serial.print(buf);
  // Serial.println(F("Scan PICC to see UID..."));
  // Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));
  while (true) {
    delay(1000);
    numCardsCorrect = 0;
    for (size_t i=0; i<NUM_OF_MFRCS; i++) {
      // delay(5);
      if ( CardPresent( *(mfrc522s[i]) )  ) {
#ifdef SERIAL_LED
        digitalWrite(LED, LOW);
#endif
        // mfrc522s[i]->PICC_HaltA();
        MFRC522::Uid &uid = mfrc522s[i]->uid;
        char uidBuf[32];
        for (size_t j=0; j<UID_BACKUPS && !correctCardTapped[i]; j++) {
          correctCardTapped[i] = uidEqual(correctUids[i][j], uid);
        }
        if (correctCardTapped[i]) {
          numCardsCorrect += 1;
        }
#ifdef SERIAL_DEBUG_OUT
        uidToStr(uidBuf, uid);
        sprintf(buf, "MFRC %d: %-29s - %s\r\n\r\n", i, uidBuf, correctCardTapped[i] ? "right" : "wrong");
        Serial.print(buf);
#endif
        /* byte ledpin = correctCardTapped[i] ? LED_GREEN : LED_RED;
        digitalWrite(ledpin, HIGH);
        delay(800);
        digitalWrite(ledpin, LOW);
        delay(200); */
      }
      else {
#ifdef SERIAL_DEBUG_OUT
        sprintf(buf, "MFRC %d: %-29s - %s\r\n\r\n", i, "no card detected", "wrong");
        Serial.print(buf);
#endif
      }
    }
#ifdef SERIAL_DEBUG_OUT
    Serial.println();
#endif
    if (numCardsCorrect >= NUM_CORRECT_CARDS_ENOUGH) {
      pinMode(RIDDLE_SOLVED_PIN, OUTPUT);
      digitalWrite(RIDDLE_SOLVED_PIN, LOW);
#ifdef SERIAL_DEBUG_OUT
      Serial.println("Level finished!\r\n");
#endif
      for (size_t i = 0; i < 3; i++) {
        digitalWrite(LED_GREEN, HIGH);
        delay(200);
        digitalWrite(LED_GREEN, LOW);
        delay(200);
      }
      delay(2000);
#ifdef SERIAL_DEBUG_OUT
      Serial.println("Restart\r\n");
#endif
      for (size_t i=0; i<NUM_OF_MFRCS; i++) {
        correctCardTapped[i] = false;
      }
      pinMode(RIDDLE_SOLVED_PIN, INPUT_PULLUP);
    }
  }
  for (size_t i=0; i < NUM_OF_MFRCS; i++) {
    delete mfrc522s[i];
  }
}
